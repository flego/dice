module Main where

import System.Random
import System.Environment
import Lib

main :: IO ()
main = do
  args <- getArgs
  let n = dismantle args
  -- putStrLn $ "dices count: " ++ show n
  res <- throwDices n
  putStrLn $ "result: " ++ show res
  
throwDices :: Int -> IO [Int]
throwDices 0 = return []
throwDices n = do
  r1 <- randomRIO(1,6)
  rs <- throwDices $ n - 1
  return $! r1 : rs
   
dismantle :: [String] -> Int
dismantle [] = error "No arguments given!"
dismantle (n:xs) = read n :: Int
